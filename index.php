<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Parking Lot Statistics</title>
  <meta name="description" content="See statistics of different parking lots"/>
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>
  <link rel="icon" sizes="192x192" href="img/favicon.png">

  <link rel="stylesheet" href="css/style.css">
</head>

<body class = "loading-data">
  <div id="svg-container">
    <?php include 'img/svg-sprite.svg'; ?>
  </div>

  <nav>
    <ul>
      <li class="logo">
        <a href="#">
          <svg viewBox="0 0 790 350" class="logo-svg">
            <use xlink:href="#logo"></use>
          </svg>
        </a>
      </li>
      <li class="nav active">
        <a href="#">
          <span class = "icon-wrapper">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#home-icon"></use>
            </svg>
          </span>
          <span>Home</span>
        </a>
      </li>
      <li class="nav">
        <a href="#config">
          <span class = "icon-wrapper">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#config-icon"></use>
            </svg>
          </span>
          <span>Config</span>
        </a>
      </li>
      <li class="nav">
        <a href="#help">
          <span class = "icon-wrapper">
            <svg viewBox="0 0 100 100" class="icon">
              <use xlink:href="#search-icon"></use>
            </svg>
          </span>
          <span>Help</span>
        </a>
      </li>
    </ul>
  </nav>

  <main>
    <header>
      <div id="notifier">
        <p></p>
        <button class="close-button">
          <svg viewBox="0 0 100 100" class="icon">
            <use xlink:href="#close-icon"></use>
          </svg>
        </button>
      </div>
      <div class="container">
        <div id="url-container">
          <svg viewBox="0 0 100 100" class="icon">
            <use xlink:href="#url-icon"></use>
          </svg>
          <span class="url"></span>
          <input type="text" id = "get-stats-url-input">
          <button id="get-parking-data">Get Data</button>
        </div>
        <a id = "mail-me" href="mailto:goga.chinchaladze@gmail.com" target="_blank">
          <svg viewBox="0 0 100 100" class="icon">
            <use xlink:href="#mail-icon"></use>
          </svg>
        </a>
      </div>
    </header>
    <div id="main-container">
      <section id = "home-section" class = "container">
        <div id="greeting">
          <h3>Hello Guest,</h3>
          <h4>Checkout the statistics of different parking lots.</h4>
        </div>

        <select id = "parking-lot-selector" class = "default-select">
          <option value="default" data-parameters = "days=5&items=500">Default Lot</option>
          <option value="heroes" data-parameters = "days=30&items=5000">Heroes Lot</option>
          <option value="villains" data-parameters = "days=90&items=200000">Villains Lot</option>
          <option value="parkHere" data-parameters = "days=2&items=30">Park Here Lot</option>
        </select>
        <div id="chart-wrapper">
          <h2>Statistics</h2>
          <div id="parking-lot-stats-chart"></div>
          <div class="loading"></div>
        </div>

        <div id="chart-results">
          <div id="all-time-max">
            <span class = "text">All time max: </span>
            <span class = "max"></span>
            <span class = "calendar-wrapper">
              <svg viewBox="0 0 100 100" class="icon">
                <use xlink:href="#calendar-icon"></use>
              </svg>
              <div class = "max-dates"></div>
            </span>
          </div>
          <div id="max-in-range">
            <span class = "text">Maximum in range:</span>
            <span class = "max"></span>
            <span class = "calendar-wrapper">
              <svg viewBox="0 0 100 100" class="icon">
                <use xlink:href="#calendar-icon"></use>
              </svg>
              <div class = "max-dates"></div>
            </span>
          </div>
        </div>
      </section>
    </div>
    <footer>
      <small>© No single right reserved.</small>
    </footer>
  </main>

  <script src="js/lib.js"></script>
  <script src="js/script.js"></script>
</body>