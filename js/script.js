(function(window, document){
  "use strict";
  var windowWidth = document.documentElement.clientWidth,
      windowHeight = document.documentElement.clientHeight;

  var htmlClassHandler = function () {
    if(!G().isTouch()){
      $('html').addClass('no-touch');
    }
  };
  var htmlEventsHandler = function () {
    document.body.addEventListener('click', function (e) {
      //close burger menu
    });
  };

  //The class will construct a notifier with functions that just shows/removes it and adds custom messages. needs elementID as parameter
  function Notifier(elId) {
    var el = document.getElementById(elId),
        $el = $('#' + elId),
        currentStatus = '',
        transitionTime = 200,
        _this = this,
        notificationDisplayingTime = 1500;

    el.addEventListener('click', function (e) {
      e.stopPropagation();
    });

    $el.find('.close-button').click(function (e) {
      e.preventDefault();
      _this.hide();
    });

    this.show = function (status, text) {
      $el.removeClass(currentStatus);
      currentStatus = status;
      $el.addClass(status);
      el.getElementsByTagName('p')[0].innerHTML = text;
      $el.addClass('visible');
    };

    this.hide = function () {
      $el.removeClass('visible');
      setTimeout(function () {
        $el.removeClass(currentStatus);
      },transitionTime + 1);
    };

    this.showAndHide = function (status, text) {
      this.show(status, text);
      (function (that) {
        setTimeout(function () {
          that.hide();
        }, notificationDisplayingTime);
      })(this);
    };
    return this;
  }
  var mainNotifier = new Notifier('notifier');


  //Module of statistics application itself
  var parkingLotStatistics = (function () {
    //TODO cacheHandler to be used with localstorage cached data, in case of server error the app will show information from cache
    var cacheHandler = (function () {
      var main = {
        'getData' : function () {

        }
      };
      return main;
    })();
    var statsDomEventsHandler = function () {

      //Get data button click handler
      document.getElementById('get-parking-data').addEventListener('click', function (e) {
        e.preventDefault();
        var params = document.getElementById('get-stats-url-input').value;
        if (params.charAt(0) == '?' || params.charAt(0) == '/') { //getting rid of extra ? if exists
          params = params.substring(1);
        }
        statsProvider.setParameters(params);

        var selectEl = document.getElementById('parking-lot-selector');
        var defaultIndex = -1;

        for(var i = 0, length = selectEl.options.length; i<length; i++){ //getting default option's index
          if(selectEl.options[i].value.toLowerCase() == 'default'){
            defaultIndex = i;
            break;
          }
        }

        if(defaultIndex >= 0){
          selectEl.options[defaultIndex].setAttribute('data-parameters', params); //Changing default select options' params

          if(selectEl.selectedIndex != defaultIndex){
            selectEl.selectedIndex = defaultIndex; //changing select to default option
          }
          else{
            main.updateData();
          }
        }
        else{
          main.updateData();
        }

      });

      //Select change handler
      document.getElementById('parking-lot-selector').addEventListener('change', function (e) {
        e.preventDefault();
        var selectedOption = this.options[this.selectedIndex];
        statsProvider.setParameters(selectedOption.getAttribute('data-parameters'));
        // statsProvider.getDataFromServer();
        main.updateData();
      });
    };
    var statsDomHandler = (function () {

      var main = {
        updateAllTimeMax : function () {
          var allTimeMax = dataHandler.getAllTimeMax();
          var allTimeMaxEl = document.getElementById('all-time-max');
          var timesUl = getUlDates(allTimeMax.times);
          allTimeMaxEl.getElementsByClassName('max-dates')[0].innerHTML = timesUl;
          allTimeMaxEl.getElementsByClassName('max')[0].innerHTML = allTimeMax.max;

          // $(allTimeMaxEl).addClass('visible');
        },
        updateMaxInRange : function (start, end, data) {
          var maxInRangeEl = document.getElementById('max-in-range');
          var timesUl = getUlDates(data.times);
          maxInRangeEl.getElementsByClassName('max-dates')[0].innerHTML = timesUl;
          maxInRangeEl.getElementsByClassName('max')[0].innerHTML = data.max;
        }
      };

      return main;

      function getFormattedDate(date) {
        var utcDateStr = (new Date(date)).toUTCString();
        utcDateStr = utcDateStr.substring(5); // remove weekday
        utcDateStr = utcDateStr.slice(0, -4); //remove GMT
        return utcDateStr;
      }
      function getUlDates(times){
        var htmlString = "<ul>";

        for(var i=0,length = times.length; i<length; i++){
          var startTimeInMinutes = Math.floor(times[i]['start'] / (60*1000));
          var endTimeInMinutes = Math.floor(times[i]['end'] / (60*1000));

          var preposition = (startTimeInMinutes == endTimeInMinutes) ? 'On' : 'Between';
          preposition += "<br/>"
          var date = (startTimeInMinutes == endTimeInMinutes) ? getFormattedDate(times[i]['start']) : getFormattedDate(times[i]['start']) + ' - ' + getFormattedDate(times[i]['end']);
          htmlString += "<li>" + preposition + " " + date + "</li>";
        }

        htmlString += "</ul>";
        return htmlString;
      }
    })();
    var statsProvider = (function () {
      var url = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '') + location.pathname;
      url += (url.charAt(url.length - 1) == '/') ? 'parking-stats.php?' : '/parking-stats.php?';
      // var url = window.location.href + 'parking-stats.php?';
      var parametersInput = document.getElementById('parking-lot-selector');
      var parameters = parametersInput.options[parametersInput.selectedIndex].getAttribute('data-parameters');
      // console.log(window.location.href)
      var main = {
        getUrl : function () {
          return url;
        },
        setUrl : function (url) {
          url = url;
        },
        getParameters : function () {
          return parameters;
        },
        setParameters : function (par) {
          parameters = par;
          document.getElementById('get-stats-url-input').value = par;
        },
        getDataFromServer : function () {
          return $.get(url + parameters);
        }
      };
      return main;
    })();
    var dataHandler = (function () {
      var _data = [];
      var _allTimeMax = [];


      var main = {
        getData : function () {
          return _data;
        },
        getAllTimeMax : function () {
          return _allTimeMax;
        },
        setAllTimeMax : function (allTimeMax) {
          _allTimeMax = allTimeMax;
          statsDomHandler.updateAllTimeMax();
        },
        setData : function (data) {
          _data = data;
        },
        processData : function (data) {
          var processingStartTime = new Date().getTime();
          var statsUnordered = {};
          var times = [];
          for(var i = 0, length = data.length; i<length; i++){
            var arrivalDate = new Date(data[i]['ArrivalTime']),
              leaveDate = new Date(data[i]['LeaveTime']),
              arrivalTimeInMilliseconds = arrivalDate.getTime(),
              /*
               * Adding 1 minute to prevent showing less cars when arrives or leaves at the same
               */
              leaveTimeInMilliseconds = leaveDate.getTime() + 1 * 60 * 1000;

            addRecordToStatsObj(statsUnordered, arrivalTimeInMilliseconds, 1);
            addRecordToStatsObj(statsUnordered, leaveTimeInMilliseconds, -1);
          }
          for(var key in statsUnordered){
            var keyToInt = parseInt(key),
                date = new Date(keyToInt);
                // timeForChart = date.setHours(date.getHours() + date.getTimezoneOffset()/60); //As the chart library is converting to local time, I had to change time manually (fixed)
            if(statsUnordered[key] != 0){
              times.push({
                'time' : keyToInt,
                'cars' : statsUnordered[key]
              });
            }
          }

          //Sort array
          times.sort(function (a, b) {
            return a.time - b.time;
          });
          var carCount = 0;
          for(var i = 0, length = times.length; i<length; i++){
            times[i]['cars'] += carCount;
            carCount = times[i]['cars'];
          }

          this.setData(times);
          this.setAllTimeMax(this.findMaxBetweenTimes());
          var processingEndTime = new Date().getTime();
          var processingTime = processingEndTime - processingStartTime;
          return times;

          function addRecordToStatsObj(obj , ms, arriveOrLeave) {
            if (ms in obj) {
              obj[ms] += arriveOrLeave;
            }
            else {
              obj[ms] = arriveOrLeave;
            }
          }
        },
        findMaxBetweenTimes : function (start, end) {
          var data = this.getData();
          start = (start !== undefined) ? start : 0;
          end = (end !== undefined) ? end : data[data.length-1]['time'];
          var maxStartIndex = 0,
            prevIndex = 0,
            maxEndIndex = 1,
            max = -1,
            maxOnTimes = []; //for several maximums

          for(var i = 0, length = data.length; i< length; i++){
            if(start < data[i]['time'] && max == -1){
              prevIndex = ((i-1) > 0) ? i-1 : 0;
              max = data[prevIndex]['cars'];
              maxStartIndex = prevIndex;
              maxEndIndex = i;
              maxOnTimes.push({
                'start' : start,
                'end' : data[maxEndIndex]['time']
              });
            }
            else if(start <= data[i]['time'] && data[i]['time']<=end){
              if(data[i]['cars'] >= max) {
                if(data[i]['cars'] > max) maxOnTimes = [];
                max = data[i]['cars'];
                maxStartIndex = i;
                maxEndIndex = i+1;
                maxOnTimes.push({
                  'start' : data[maxStartIndex]['time'],
                  'end' : (data.length > i+1) ? data[maxEndIndex]['time'] : end
                });
              }
            }
          }
          var objToReturn = {max : max, times : maxOnTimes};
          statsDomHandler.updateMaxInRange(start, end, objToReturn);
          return objToReturn;
        }
      };
      return main;
    })();
    var chartHandler = (function () {
      var chart;

      var main = {
        getChart : function () {
          return chart;
        },
        update : function (data) {
          chart.dataProvider = data;
          chart.validateNow(true, false);
        },
        init : function (data) {
          // options.dataProvider = data;
          if(AmCharts.isReady){
            generateChart();
          }
          else{
            AmCharts.ready(generateChart);
          }

          function generateChart() {
            AmCharts.useUTC = true;
            AmCharts.theme = AmCharts.themes.light;
            chart = new AmCharts.AmSerialChart();

            chart.dataProvider = data;
            chart.categoryField = "time";
            chart.title = "Parking Lot Statistics";

            chart.mouseWheelScrollEnabled = true;
            chart.mouseWheelZoomEnabled = true;
            chart.autoMarginOffset= 20;
            chart.chartScrollbar = {
              "autoGridCount": true,
              "graph": "g1",
              "scrollbarHeight": 40
            };
            chart.chartCursor = {
              "limitToGraph":"g1",
              "categoryBalloonDateFormat" : 'MMM DD, HH:NN',
              // categoryBalloonEnabled : false
            };
            var graph = new AmCharts.AmGraph();
            graph.id = "g1";
            graph.valueField = "cars";
            graph.type = "line";
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);


            var categoryAxis = chart.categoryAxis;
            // categoryAxis.gridCount = chartData.length;
            categoryAxis.parseDates = true;
            // categoryAxis.gridPosition = "start";
            categoryAxis.labelRotation = 90;
            categoryAxis.minPeriod = "mm";
            // categoryAxis.minorGridEnabled= true;
            // categoryAxis.markPeriodChange = false;
            categoryAxis.showLastLabel = true;
            // categoryAxis.boldPeriodBeginning = false;

            chart.addListener('zoomed',function (e) {
              var data = dataHandler.getData(),
                  startTime = data[e.startIndex]['time'],
                  endTime = data[e.endIndex]['time'];
              dataHandler.findMaxBetweenTimes(startTime, endTime);
            });

            chart.addListener('rendered',function () {
              chart.zoomToIndexes(0, dataHandler.getData().length);
              $('body').removeClass('loading-data');
            });

            chart.write('parking-lot-stats-chart');
          }
        }
      };
      return main;
    })();
    var main = {
      init : function () {
        statsDomEventsHandler();
        document.querySelector('#url-container .url').innerHTML = statsProvider.getUrl();
        document.getElementById('get-stats-url-input').value = statsProvider.getParameters();
        //TODO if cached data exists, display
        this.updateData();
      },
      updateData : function () {
        mainNotifier.show('wait','Getting data from server...');
        $('body').addClass('loading-data');
        statsProvider.getDataFromServer()
          .done(this.render)
          .fail(function (err) {
            mainNotifier.showAndHide('error',err.responseText);
            console.log(err);
            $('body').removeClass('loading-data');
          });
      },
      render : function (data) {
        //Add data to cache
        var processingStarted = new Date().getTime();
        if(!JSON.parse(data).length){
          return;
        }
        $.when(dataHandler.processData(JSON.parse(data)))
          .done(function(data) {
            var processingFinished = new Date().getTime();
            var processingTime = processingFinished-processingStarted;
            mainNotifier.showAndHide('success','Data processing finished! (' + processingTime + ' ms)');
            console.log('Processing time: ' + processingTime);
            if(chartHandler.getChart()){
              chartHandler.update(data);
            }
            else{
              chartHandler.init(data);
            }
          })
          .fail(function (err) {
            mainNotifier.showAndHide('error','Error while processing data :(');
            console.log(err);
            $('body').removeClass('loading-data');
          });
      }
    };
    return main;
  })();


  var main = function(){
    parkingLotStatistics.init();
  };

  document.addEventListener('DOMContentLoaded', function(){
    main();
  });

  window.addEventListener('load', function(e) {
    $('html').addClass("loaded");
  });
})(window, document);



