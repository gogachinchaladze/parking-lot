<?php
$max_number_of_days = 90;
$number_of_days = 1;

//http://parkingapi.gear.host/v1/parking
if (isset($_GET['api_url'])) {
  $api_url = $_GET['api_url'] . (strpos($_GET['api_url'], '?') ? '' : '?'); //Adding ? mark if api url doesn't have one
} else {
  $api_url = "http://parkingapi.gear.host/v1/parking?";
}

if (isset($_GET['items'])) {
  $api_url .= "items=" . $_GET['items'] . '&';
}

if (isset($_GET['all_time']) && $_GET['all_time']) {
  $number_of_days = $max_number_of_days;
} else if (isset($_GET['start_date'])) {
  $start_date = $_GET['start_date'];
  $end_date = (isset($_GET['end_date'])) ? $_GET['end_date'] : round(time() * 1000);
  $number_of_days = floor(($end_date - $start_date) / (24 * 60 * 60 * 1000));
} else if (isset($_GET['days']) && $_GET['days']) {
  $number_of_days = $_GET['days'];
}

$api_url .= "days=$number_of_days";

//  $statistics = file_get_contents($api_url);
$statistics = @file_get_contents($api_url);


if(!count(json_decode($statistics))){
  header("HTTP/1.0 500 Internal Server Error");
  echo "No data for current parameters!";
}
else if ($statistics) {
  echo $statistics;
}
else {
  header("HTTP/1.0 500 Internal Server Error");
  echo "Cannot get data from the url provided";
}

?>